<?php
/*
If you would like to edit this file, copy it to your current theme's directory and edit it there.
WPUF will always look in your theme's directory first, before using this default template.
*/
?>
<div class="login" id="user-reset-form">

    <?php FRONTEND_USER()->login->show_errors(); ?>


	<form name="resetpasswordform" id="resetpasswordform" action="<?php echo site_url('login/?action=resetpass', 'login_post') ?>" class="displayform_class" method="post">
	    <?php FRONTEND_USER()->login->show_messages(); ?>
	    <ul class="user-form">
	    	<fieldset class="user-el 0 user_email feup_top">         
	    		<div class="user-label">
	    			<label for="user-user_new_pass"><?php _e( 'New password' ); ?> <span class="frontend-required-indicator">*</span>
	    			</label>
	    			<br>
	    		</div>
	    		<div class="user-fields">
	    			<input autocomplete="off" name="pass1" id="user-pass1" class="input" size="40"  value="" type="password" autocomplete="off"  />
	    		</div>
	    	</fieldset>
	    	<fieldset class="user-el 0 user_email feup_top">         
	    		<div class="user-label">
	    			<label for="user-user_new_pass"><?php _e( 'Confirm new password' ); ?><span class="frontend-required-indicator">*</span>
	    			</label>
	    			<br>
	    		</div>
	    		<div class="user-fields">
	    			<input autocomplete="off" name="pass2" id="user-pass2" class="input" size="40"  value="" type="password" autocomplete="off"  />
	    		</div>
	    	</fieldset>
	    	<?php do_action( 'resetpassword_form' ); ?>
	    	<fieldset>
	    		<input type="submit" name="wp-submit" id="wp-submit" value="<?php esc_attr_e( 'Reset Password' ); ?>" />
				<input type="hidden" name="key" value="<?php echo FRONTEND_USER()->login->get_posted_value( 'key' ); ?>" />
				<input type="hidden" name="login" id="user_login" value="<?php echo FRONTEND_USER()->login->get_posted_value( 'login' ); ?>" />
				<input type="hidden" name="user_reset_password" value="true" />                 
	    	</fieldset>
	   	</ul>
		<?php wp_nonce_field( 'user_reset_pass' ); ?>
	</form>
</div>

