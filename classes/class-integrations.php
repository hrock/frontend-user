<?php
if ( !defined( 'ABSPATH' ) ) {
	exit;
}
class USER_Integrations {
	function __construct() {
    add_filter( 'frontend_download_supports', array(  $this, 'enable_reviews') );
    add_action('user_register',  array($this ,'member_register'));
    add_action('init',  array($this ,'member_check'));
    remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_login_form', 10 );
    add_action('woocommerce_before_checkout_form', array($this, 'render_login_reg_form'));
    add_action( 'wp_ajax_feup_login_woo_ajax', array( $this, 'feup_login_woo_ajax_login') );
    add_action( 'wp_ajax_nopriv_feup_login_woo_ajax', array( $this, 'feup_login_woo_ajax_login' ) );
    add_action( 'wp_ajax_feup_reg_woo_ajax', array( $this, 'feup_reg_woo_ajax_reg') );
    add_action( 'wp_ajax_nopriv_feup_reg_woo_ajax', array( $this, 'feup_reg_woo_ajax_reg' ) );
	}

  // function feup_reg_woo_ajax_reg() 
  // {
  //     if (isset($_POST['pid'])) {
  //       $form_id = $_POST['pid'];
  //         echo do_shortcode('[wpfeup-register type="registration" id="'.$form_id.'"]');
  //     }
  //     exit();
  // }
  // function feup_login_woo_ajax_login() 
  // {
  //     if (isset($_POST['pid'])) {
  //       $form_id = $_POST['pid'];
  //        // echo do_shortcode('[wpfeup-login id="'.$form_id.'"]');
  //     }
  //     exit();
  // }
  function render_login_reg_form() 
  {
    $form_id = get_option('user-woocommcerce-login-url');
    $form_id_reg = get_option('user-woocommcerce-reg-url');
    $front_login = get_option('user-woocommcerce-login-replace');
    if ($front_login == 1 && !is_user_logged_in()) 
    {
      if ($form_id) 
      { ?>
        <div class="woocommerce-info x-alert x-alert-info x-alert-block">
          Returning customer? 
          <a class="showfrontendlogin" href="#">Click here to login</a>  
        </div>
        <div class='frontent_login'><?php echo do_shortcode('[wpfeup-login id="'.$form_id.'"]'); ?></div>
        <script type="text/javascript">
          var $ = jQuery;
          $(document).ready(function(){
            // $('.frontent_login').empty();
            $('.showfrontendlogin').on('click', function(e){
              e.preventDefault();
              var form_id = '<?php echo $form_id; ?>';
              $('.frontent_login').toggle();
            })
          });
        </script>
        <style type="text/css">
          .frontent_login{
            display: none;
          }
        </style>
        <?php
      }
      if ($form_id_reg) 
      {?>
        <div class="woocommerce-info x-alert x-alert-info x-alert-block">
          For Unregistered customer? 
          <a class="showfrontendreg" href="#">Click here to Register</a>  
        </div>
        <div class='frontent_reg_woo'><?php echo do_shortcode('[wpfeup-register type="registration" id="'.$form_id_reg.'"]'); ?></div>
        <script type="text/javascript">
          var $ = jQuery;
          $(document).ready(function(){
            // $('.frontent_reg_woo').empty();
            $('.showfrontendreg').on('click', function(e){
              e.preventDefault();
              var form_id = '<?php echo $form_id; ?>';
              $('.frontent_reg_woo').toggle();
            })
          });
        </script>
        <style type="text/css">
          .frontent_reg_woo{
            display: none;
          }
        </style> <?php
      }    
    }
  }

  public static function member_register($user_id) 
  {
      global $wp_roles;
      $u = new WP_User($user_id );
      $role = implode(',', $u->roles);
      $cur_time=date("Y-m-d H:i:s");
      add_user_meta($user_id , '_frontend_user_time', $cur_time);
  }
  public static function member_check() 
  {
    global $wpdb;
    $tb2 =$wpdb->prefix."feup_member_payment";
    $sql = $wpdb->get_results("select * from $tb2");
    foreach ($sql as $key1 => $value1) 
    {
      $tb = $wpdb->prefix."user_member_list";
      $tb1 = $wpdb->get_results("SELECT * FROM $tb WHERE id = '".$value1->level_id."' ;");
      $expire = $value1->paydate;
      $user_id = $value1->user_id;
      foreach ($tb1 as $tb2) 
      {
        $time = $tb2->access_limited_time_value;
        $new_role = $tb2->afterexpire_level;
        $role = $tb2->level_slug;
        if($expire )
        {
          if($tb2->access_type == 'limited')
          {
            if ($tb2->access_limited_time_type == 'W') 
            {
              $time_inter = '+'.$time.' weeks';
            }elseif ($tb2->access_limited_time_type == 'M') 
            {
              $time_inter = '+'.$time.' months';
            }elseif ($tb2->access_limited_time_type == 'Y') 
            {
              $time_inter = '+'.$time.' years';
            }else
            {
              $time_inter = '+'.$time.' days';
            }
            $expire = strtotime($expire);
            $new_time = strtotime($time_inter, $expire);
            $cur_time=date("Y-m-d H:i:s");
            $cur_time = strtotime($cur_time);
            if($new_time <= $cur_time)
            {
              $user_id = wp_update_user( array( 'ID' => $user_id, 'role' => $new_role ) );   
            }
          }elseif ($tb2->access_type == 'date_interval') 
          {
            $role = $tb2->level_slug;
            $start = $tb2->access_interval_start;
            $end   = $tb2->access_interval_end;
            $start = strtotime($start);
            $end = strtotime($end);
            $cur_time=date("Y-m-d H:i:s");
            $cur_time = strtotime($cur_time);
            if($start <= $cur_time)
            {
                  $user_id = wp_update_user( array( 'ID' => $user_id, 'role' => $role ) );   
            }
            if($end <= $cur_time)
            {
                  $user_id = wp_update_user( array( 'ID' => $user_id, 'role' => $new_role ) );   
            }
          }elseif($tb2->access_type == 'regular_period')
          {
            if ($value1->access_limited_time_type == 'W') 
            {
              $time_inter = '+'.$time.' weeks';
            }elseif ($value1->access_limited_time_type == 'M') 
            {
              $time_inter = '+'.$time.' months';
            }elseif ($value1->access_limited_time_type == 'Y') 
            {
              $time_inter = '+'.$time.' years';
            }else
            {
              $time_inter = '+'.$time.' days';
            }
            $expire = strtotime($expire);
            $new_time = strtotime($time_inter, $expire);
            $cur_time=date("Y-m-d H:i:s");
            $cur_time = strtotime($cur_time);
            if($new_time <= $cur_time)
            {
                 $user_id = wp_update_user( array( 'ID' => $user_id, 'role' => $new_role ) );   
            }
          }
        }
      }
    }
  }
  public function enable_reviews( $supports ) 
  {
   return array_merge( $supports, array(        'reviews'         ) );
  }
  public static function is_commissions_active() {
    if ( !defined( 'FRONTENDC_PLUGIN_DIR' ) ) {
         return false;
    } else {
         return true;
    }
  }
}